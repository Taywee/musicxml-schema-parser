use crate::error::Result;
use std::io::Write;

/** Trait for types that can write out to Rust code.
 */
pub trait WriteRust {
    fn write_rust<O: AsMut<dyn Write>>(&self, output: &mut O) -> Result<()>;
}
