use minidom;
use std::convert::From;
use std::error;
use std::fmt;
use std::io;
use std::num;
use std::result;

/// An error in loading data from an ascii song
#[derive(Debug)]
pub enum Error {
    Structure(String),
    Io(io::Error),
    Minidom(minidom::Error),
    ParseInt(num::ParseIntError),
}

impl From<String> for Error {
    fn from(error: String) -> Self {
        Error::Structure(error)
    }
}

impl From<&str> for Error {
    fn from(error: &str) -> Self {
        Error::Structure(String::from(error))
    }
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::Io(error)
    }
}

impl From<minidom::Error> for Error {
    fn from(error: minidom::Error) -> Self {
        Error::Minidom(error)
    }
}

impl From<num::ParseIntError> for Error {
    fn from(error: num::ParseIntError) -> Self {
        Error::ParseInt(error)
    }
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Structure(e) => write!(f, "Structure Error: {}", e),
            Error::Io(e) => write!(f, "IO Error: {}", e),
            Error::Minidom(e) => write!(f, "Minidom Error: {}", e),
            Error::ParseInt(e) => write!(f, "ParseInt Error: {}", e),
        }
    }
}

pub type Result<T> = result::Result<T, Error>;
