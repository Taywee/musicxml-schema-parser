use crate::error::Error;
use crate::error::Result;
use crate::write_rust::WriteRust;
use minidom::element::Element;
use std::convert::From;
use std::convert::TryFrom;
use std::fmt;
use std::io::Write;

#[derive(Debug)]
enum RustType {
    Struct,
    Enum,
}

impl fmt::Display for RustType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                RustType::Struct => "struct",
                RustType::Enum => "enum",
            }
        )
    }
}

#[derive(Debug, Default)]
struct Restriction<'a> {
    base: &'a str,
    enumeration: Vec<&'a str>,
    min_inclusive: Option<i64>,
    max_inclusive: Option<i64>,
    min_exclusive: Option<i64>,
    max_exclusive: Option<i64>,
    pattern: Option<&'a str>,
    min_length: Option<u64>,
}

impl Restriction<'_> {
    fn rust_type(&self) -> RustType {
        if self.enumeration.is_empty() {
            RustType::Struct
        } else {
            RustType::Enum
        }
    }

    fn write_rust_body<O: AsMut<dyn Write>>(&self, output: &mut O) -> Result<()> {
        let writer = output.as_mut();
        match self.rust_type() {
            RustType::Struct => {
                writeln!(writer, "(pub {});", self.base.type_name())?;
            }
            RustType::Enum => {
                writeln!(writer, "{}", " {")?;
                for member in &self.enumeration {
                    writeln!(writer, "    {},", member.enum_name())?;
                }
                writeln!(writer, "{}", "}")?;
            }
        }
        Ok(())
    }
}

#[derive(Debug, Default)]
struct Union<'a> {
    member_types: Vec<&'a str>,
}

impl Union<'_> {
    fn write_rust_body<O: AsMut<dyn Write>>(&self, output: &mut O) -> Result<()> {
        let writer = output.as_mut();
        writeln!(writer, "{}", " {")?;
        for member_type in &self.member_types {
            writeln!(
                writer,
                "    {}({}),",
                member_type.enum_name(),
                member_type.type_name()
            )?;
        }
        writeln!(writer, "{}", "}")?;
        Ok(())
    }
}

#[derive(Debug)]
enum Type<'a> {
    Restriction(Restriction<'a>),
    Union(Union<'a>),
}

impl Type<'_> {
    fn rust_type(&self) -> RustType {
        match self {
            Type::Restriction(restriction) => restriction.rust_type(),
            Type::Union(_) => RustType::Enum,
        }
    }

    fn write_rust_body<O: AsMut<dyn Write>>(&self, output: &mut O) -> Result<()> {
        match self {
            Type::Restriction(restriction) => restriction.write_rust_body(output),
            Type::Union(union) => union.write_rust_body(output),
        }
    }
}

#[derive(Debug)]
pub struct SimpleType<'a> {
    name: String,
    annotation: Option<&'a str>,
    type_: Type<'a>,
}

/// Used to allow a type to return itself as a Rust type name or enum identifier.
trait TypeName {
    fn type_name(&self) -> String;
    fn enum_name(&self) -> String;
    fn to_pascal_case(&self) -> String;
}

impl<T> TypeName for T
where
    T: AsRef<str>,
{
    fn type_name(&self) -> String {
        match self.as_ref() {
            "xs:token" | "xs:string" | "xs:NMTOKEN" | "xs:date" => String::from("String"),
            "xs:positiveInteger" | "xs:nonNegatvieInteger" => String::from("u32"),
            "xs:decimal" | "xs:integer" => String::from("i32"),
            _ => self.to_pascal_case(),
        }
    }

    fn enum_name(&self) -> String {
        match self.as_ref() {
            "xs:token" | "xs:string" | "xs:NMTOKEN" | "xs:date" => String::from("String"),
            "xs:positiveInteger" | "xs:nonNegatvieInteger" => String::from("U32"),
            "xs:decimal" | "xs:integer" => String::from("I32"),
            "" => String::from("Empty"),
            _ => self.to_pascal_case(),
        }
    }

    fn to_pascal_case(&self) -> String {
        let mut output = String::new();
        for part in self.as_ref().split(|c| c == '-' || c == ' ' || c == '_') {
            let mut chars = part.chars();
            if let Some(c) = chars.next() {
                output.extend(c.to_uppercase().chain(chars));
            }
        }
        if let Some(first) = output.chars().next() {
            if !first.is_ascii_alphabetic() {
                output.insert(0, 'N');
            }
        }
        output
    }
}

impl TypeName for SimpleType<'_> {
    fn type_name(&self) -> String {
        self.name.type_name()
    }

    fn enum_name(&self) -> String {
        self.name.enum_name()
    }

    fn to_pascal_case(&self) -> String {
        self.name.to_pascal_case()
    }
}

impl<'a> TryFrom<&'a Element> for Restriction<'a> {
    type Error = Error;

    fn try_from(element: &'a Element) -> Result<Self> {
        let mut output = Restriction {
            base: element
                .attr("base")
                .ok_or_else(|| Error::from("restriction needs a base"))?,
            ..Default::default()
        };
        for child in element.children() {
            if child.ns().as_deref() == Some("http://www.w3.org/2001/XMLSchema") {
                let value = child
                    .attr("value")
                    .ok_or_else(|| Error::from("restriction child needs a value"))?;
                match child.name() {
                    "enumeration" => output.enumeration.push(value),
                    "minLength" => output.min_length = Some(value.parse()?),
                    "minInclusive" => output.min_inclusive = Some(value.parse()?),
                    "minExclusive" => output.min_exclusive = Some(value.parse()?),
                    "maxInclusive" => output.max_inclusive = Some(value.parse()?),
                    "maxExclusive" => output.max_exclusive = Some(value.parse()?),
                    "pattern" => output.pattern = Some(value),
                    x => panic!("Could not handle type {}", x),
                }
            }
        }
        Ok(output)
    }
}

impl<'a> TryFrom<&'a Element> for Union<'a> {
    type Error = Error;

    fn try_from(element: &'a Element) -> Result<Self> {
        let member_types = element
            .attr("memberTypes")
            .ok_or_else(|| Error::from("need 'memberTypes' attribute for Union"))?
            .split(' ')
            .filter(|s| !s.is_empty())
            .collect();
        Ok(Union { member_types })
    }
}

impl<'a> TryFrom<&'a Element> for Type<'a> {
    type Error = Error;

    fn try_from(element: &'a Element) -> Result<Self> {
        if let Some(child) = element.get_child("restriction", "http://www.w3.org/2001/XMLSchema") {
            Ok(Type::Restriction(Restriction::try_from(child)?))
        } else if let Some(child) = element.get_child("union", "http://www.w3.org/2001/XMLSchema") {
            Ok(Type::Union(Union::try_from(child)?))
        } else {
            Err(Error::from("SimpleType needs a restriction or a union"))
        }
    }
}

impl<'a> TryFrom<&'a Element> for SimpleType<'a> {
    type Error = Error;

    fn try_from(element: &'a Element) -> Result<Self> {
        Ok(SimpleType {
            name: element
                .attr("name")
                .ok_or_else(|| Error::from("need 'name' attribute for SimpleType"))?
                .to_string(),
            annotation: element
                .get_child("annotation", "http://www.w3.org/2001/XMLSchema")
                .and_then(|annotation| {
                    annotation.get_child("documentation", "http://www.w3.org/2001/XMLSchema")
                })
                .map(Element::texts)
                .and_then(|mut i| i.next()),
            type_: Type::try_from(element)?,
        })
    }
}

impl SimpleType<'_> {
    pub fn name(&self) -> &str {
        &self.name
    }
}

impl WriteRust for SimpleType<'_> {
    fn write_rust<O: AsMut<dyn Write>>(&self, output: &mut O) -> Result<()> {
        {
            let writer = output.as_mut();
            if let Some(annotation) = &self.annotation {
                for line in annotation.lines() {
                    writeln!(writer, "/// {}", line)?;
                }
            }
            write!(
                writer,
                "pub {} {}",
                self.type_.rust_type(),
                self.type_name()
            )?;
        }
        self.type_.write_rust_body(output)?;
        writeln!(output.as_mut(), "")?;
        Ok(())
    }
}
