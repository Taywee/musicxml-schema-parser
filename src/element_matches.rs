use crate::error;
use crate::error::Result;
use minidom::Element;

pub trait ElementMatches {
    fn matches(&self, ns: &str, tag: &str) -> bool;
    fn match_or_err(&self, ns: &str, tag: &str) -> Result<()>;
}

impl ElementMatches for Element {
    fn matches(&self, namespace: &str, name: &str) -> bool {
        if let Some(self_namespace) = self.ns() {
            if namespace != self_namespace {
                return false;
            }
            if name != self.name() {
                return false;
            }
        } else {
            return false;
        }
        true
    }

    fn match_or_err(&self, namespace: &str, name: &str) -> Result<()> {
        if let Some(self_namespace) = self.ns() {
            if namespace != self_namespace {
                return Err(error::Error::from(format!(
                    "schema should be namespace {}, not {}",
                    namespace, self_namespace
                )));
            }
            if name != self.name() {
                return Err(error::Error::from(format!(
                    "tag should be {}, not {}",
                    name,
                    self.name()
                )));
            }
        } else {
            return Err(error::Error::from("Root element needs namespace"));
        }
        Ok(())
    }
}
