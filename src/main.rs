use clap::{App, Arg};
use musicxml_schema_parser::convert_schema_to_rust;
use musicxml_schema_parser::Result;
use std::fs::File;
use std::io;
use std::io::prelude::*;

fn main() -> Result<()> {
    let matches = App::new("musicxml-schema-parser")
        .version("0.1.0")
        .author("Taylor C. Richberger <taywee@gmx.com>")
        .about("Parses a subset of xsd, simply for the purpose of building a parser for MusicXML.")
        .arg(
            Arg::with_name("input")
                .short("i")
                .long("input")
                .value_name("FILE")
                .help("Takes the input musicxml xsd file (defaults to stdin).")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("output")
                .short("o")
                .long("output")
                .value_name("FILE")
                .help("Specify the output Rust file (defaults to stdout).")
                .takes_value(true),
        )
        .get_matches();

    let input = matches.value_of("input").map_or_else(
        || Ok(Box::new(io::stdin()) as Box<dyn Read>),
        |filename| {
            let result: io::Result<Box<dyn Read>> = if filename == "-" {
                Ok(Box::new(io::stdin()))
            } else {
                Ok(Box::new(File::open(filename)?))
            };
            result
        },
    )?;

    let output = matches.value_of("output").map_or_else(
        || Ok(Box::new(io::stdout()) as Box<dyn Write>),
        |filename| {
            let result: io::Result<Box<dyn Write>> = if filename == "-" {
                Ok(Box::new(io::stdout()))
            } else {
                Ok(Box::new(File::create(filename)?))
            };
            result
        },
    )?;

    convert_schema_to_rust(input, output)
}
