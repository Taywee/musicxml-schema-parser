mod element_matches;
pub mod error;
mod simple_type;
mod write_rust;

pub use crate::error::Result;

use crate::element_matches::ElementMatches;
use crate::simple_type::SimpleType;
use crate::write_rust::WriteRust;
use minidom::Element;
use quick_xml::Reader;
use std::convert::TryFrom;
use std::io::BufReader;
use std::io::Read;
use std::io::Write;

pub fn convert_schema_to_rust<I: AsMut<dyn Read>, O: AsMut<dyn Write>>(
    mut input: I,
    mut output: O,
) -> Result<()> {
    let input_bufreader = BufReader::new(input.as_mut());
    let mut reader = Reader::from_reader(input_bufreader);
    let dom = Element::from_reader(&mut reader)?;

    dom.match_or_err("http://www.w3.org/2001/XMLSchema", "schema")?;

    {
        let writer = output.as_mut();
        writeln!(
            writer,
            "{}",
            r#"
        pub trait SimpleType<Base>: TryFrom<AsRef<str>> {
            fn value(&self) -> Base;
        }

        impl<Base> From<AsRef<SimpleType<Base>>> for Base {
            fn from(simple_type: AsRef<SimpleType<Base>>) -> Base {
                simple_type.as_ref().value()
            }
        }
        "#
        )?;
    }

    for child in dom.children() {
        if child.matches("http://www.w3.org/2001/XMLSchema", "simpleType") {
            let simple_type = SimpleType::try_from(child)?;
            simple_type.write_rust(&mut output)?;
        }
    }

    Ok(())
}
