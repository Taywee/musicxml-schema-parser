# musicxml-schema-parser

Very specific-purpose tool that parses the MusicXML schema into useable types
and some parser utility implementation for creating a MusicXML parsing library.

Effectively, this is a tool that parses an XML schema and converts it into the
types needed and hopefully the majority of the parsing needed to parse and
hopefully write that format.  This could be genericized to work for arbitrary
XML schemas, but that is not a goal of this project.  This is explicitly and
solely for converting the MusicXML schema, and will very possibly and very
likely break if used unmodified for arbitrary schemas.
